package goshe

//GO Script HElpers

import "os"

//Mkdir creates a path
func Mkdir(paths ...string) error {
	for _, path := range paths {
		os.Mkdir(path, 0755)
	}
	return nil
}

//Rm removes a path
func Rm(paths ...string) error {
	for _, path := range paths {
		os.RemoveAll(path)
	}
	return nil
}

//Exists tests if file or dir exists
func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

//Mv moves files or directories
func Mv(from, to string) error {
	err := CopyDir(from, to)
	if err != nil {
		return err
	}
	Rm(from)
	return nil
}
