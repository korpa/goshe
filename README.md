# goshe - GO Script HElpers #

## File / dir operations

### `Mkdir(paths ...string) error`
### `Rm(paths ...string) error`
### `Exists(path string) (bool, error)`
### `Mv(from, to string) error`

## Copy

### `CopyFile(src string, dst string) (error)`

CopyFile copies the contents of the file named src to the file named
by dst. The file will be created if it does not already exist. If the
destination file exists, all it's contents will be replaced by the contents
of the source file. The file mode will be copied from the source and
the copied data is synced/flushed to stable storage.

### `CopyDir(src string, dst string) (error)`

CopyDir recursively copies a directory tree, attempting to preserve permissions.
Source directory must exist, destination directory must *not* exist.
Symlinks are ignored and skipped.


## Git

### `GetCommitID(length int) (string, error)`

## Run

## Tar & Zip

### `TarDir(src string, buf io.Writer) error`

Tar a directory and gzip it

### `ReadZipFile(zf *zip.File) ([]byte, error)`

Unzip a file