package goshe

//GetCommitID gets the commit ID
func GetCommitID(length int) (string, error) {
	commitIDBytes, err := RunError("git", "rev-parse", "HEAD")
	if err != nil {
		return "", err

	}

	commitID := string(commitIDBytes)
	commitIDshort := commitID[:length]
	return commitIDshort, nil

}
