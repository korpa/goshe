package goshe

import (
	"os"

	"github.com/fatih/color"
)

//GoPathSet tests if the GOPATH environment var is set
func GoPathSet() {
	if os.Getenv("GOPATH") == "" {
		color.Red("GOPATH not set")
		os.Exit(1)
	}
}

//GetGoExecutable returns the bath to the go binary
func GetGoExecutable() (string, error) {
	_, err := RunError("go", "version")
	if err == nil {
		return "go", nil
	}
	color.Yellow("Go not found in PATH. Check if GOROOT is set")

	if os.Getenv("GOROOT") == "" {
		color.Red("Can not find Go in PATH, nor GOROOT is set.")
		os.Exit(1)
	}
	color.Green("GOROOT set => build should work")
	return os.Getenv("GOROOT") + "/bin/go", nil
}
