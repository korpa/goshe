package goshe

import (
	"bytes"
	"os"
	"os/exec"
)

//GO Script HElpers

//Run runs a command
func Run(cmd string, args ...string) error {
	//	fmt.Printf("%+v\n", args)
	rcmd := exec.Command(cmd, args...)
	rcmd.Stdout = os.Stdout
	rcmd.Stderr = os.Stderr
	err := rcmd.Run()
	if err != nil {
		return err
	}
	return nil
}

//RunInWorkingDir runs a command in a specific working directory
func RunInWorkingDir(workingDir string, cmd string, args ...string) error {
	//	fmt.Printf("%+v\n", args)
	rcmd := exec.Command(cmd, args...)
	rcmd.Stdout = os.Stdout
	rcmd.Stderr = os.Stderr
	rcmd.Dir = workingDir
	err := rcmd.Run()
	if err != nil {
		return err
	}
	return nil
}

//RunEnv runs a command with set environment variables
func RunEnv(envVars []string, cmd string, args ...string) error {
	//	fmt.Printf("%+v\n", args)
	rcmd := exec.Command(cmd, args...)
	rcmd.Env = os.Environ()
	rcmd.Env = append(rcmd.Env, envVars...)
	rcmd.Stdout = os.Stdout
	rcmd.Stderr = os.Stderr
	err := rcmd.Run()
	if err != nil {
		return err
	}
	return nil
}

//RunError runs a command and returns error
func RunError(cmd string, args ...string) ([]byte, error) {
	ecmd := exec.Command(cmd, args...)
	bs, err := ecmd.CombinedOutput()
	if err != nil {
		return nil, err
	}

	return bytes.TrimSpace(bs), nil
}
